from pages.home_page import HomePage
from tests.base_test import BaseTest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class HomeTests(BaseTest):

    def test_search(self):
        home = HomePage(self.driver)
        home.open()

        home.send_to_search_field("t-shirts")

        new_page_title = WebDriverWait(self.driver, 35,
                                       EC.element_to_be_clickable((By.XPATH, "//h1[text()='Search'"))
                                       )
        assert new_page_title

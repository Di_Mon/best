from selenium.webdriver.common.by import By
from driver.driver_manager import DriverManager
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.base_page import BasePage


class HomePage(BasePage):

    driver = DriverManager().get_path_to_chromedriver()

    #locators
    _search_field = "search_query_top"
    _search_icon = "submit_search"

    def send_data_to_search_field(self, request):
        search_field = self.driver.find_element(By.ID, self._search_field)
        search_field.send_keys(request)

    def click_search_icon(self):
        search_icon = self.driver.find_element(By.NAME, self._search_icon)
        search_icon.click()

    def send_to_search_field(self, request):
        self.send_data_to_search_field(request)
        self.click_search_icon()

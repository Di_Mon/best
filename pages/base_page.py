class BasePage:

    base_url = "http://automationpractice.com/"

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        self.driver.maximize_window()
        self.driver.get(self.base_url)

#Вот здесь вообще нихуя не понятно

import hashlib
import io
import os
import platform
import zipfile

import requests

"""
For update Chromedriver in further
use official link:
https://sites.google.com/a/chromium.org/chromedriver/downloads
Go to latest release and replace URL value below for proper version.
Hashes variable also originate from that link.
"""

URL = 'https://chromedriver.storage.googleapis.com/2.27/chromedriver_{}.zip'

PLATFORMS = {
    'Linux32': ('linux32', '980387b5885be8f69343ba9f11cc7a9f'),
    'Linux64': ('linux64', 'c6d21c8fecf8bd0b880b2c36692153ef'),
    'Darwin': ('mac64', '56d908397af997f04fab32c05a26b994'),
    'Windows': ('win32', '2125188a206e2258364c3e46f07724e5'),
}


def download_and_extract():
    platform_name = platform.system()
    if platform_name == 'Linux':
        if platform_name == 'Linux' and platform.uname()[4] == 'x86_64':
            platform_name = 'Linux64'
        else:
            platform_name = 'Linux32'

    try:
        id, hash = PLATFORMS[platform_name]
    except KeyError:
        print('Platform {} not supported'.format(platform_name))
        return

    url = URL.format(id)
    print('Downloading {}'.format(url))

    request = requests.get(url)
    etag = hashlib.md5(request.content).hexdigest()
    if etag != hash:
        print('Incorrect checksum')
        return

    zipfile_to_extract = zipfile.ZipFile(io.BytesIO(request.content))
    zipfile_to_extract.extractall('.', ['chromedriver.exe'])

    os.chmod('chromedriver.exe', 0o755)


if __name__ == '__main__':
    download_and_extract()

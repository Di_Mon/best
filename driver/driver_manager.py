import os


class DriverManager():

    @classmethod
    def get_path_to_chromedriver(cls):
        path = os.path.dirname(os.path.realpath(__file__)) + '/chromedriver'
        return path
